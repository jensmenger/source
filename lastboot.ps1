Set-ExecutionPolicy -ExecutionPolicy Unrestricted -Scope Process -force
Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$false
Install-Module -Name VMware.PowerCLI -force
Write-Host "Please enter your SPIHOST\first.last Credentials for vCenter [BTH-ESXADM.spihost.team]"
Write-Host "Please review this data in the c:\LastBootUpTime-vms.csv export file on your machine..."
Connect-VIServer -Server BTH-ESXADM.spihost.team
Get-DataCenter Bethlehem | Get-VM |  where {$_.PowerState -eq "PoweredOn"} |  Get-Stat -Stat sys.uptime.latest -MaxSamples 1 -Realtime |  select Entity, @{Name="Boottime";  Expression={(Get-Date).AddSeconds(- $_.value).ToString("yy/MM/dd HH:mm:ss")}} | Export-Csv -Path c:\LastBootUpTime-vms.csv -NoTypeInformation
notepad c:\LastBootUpTime-vms.csv
import-csv "c:\LastBootUpTime-vms.csv" | ConvertTo-Json | Add-Content -Path "output.json"
