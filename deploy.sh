sudo apt update
sudo apt install -y git build-essential
sudo apt install -y ansible 
ansible --version
sudo apt install -y apt-transport-https ca-certificates curl software-properties-common gnupg-agent
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt update
sudo apt-cache policy docker-ceudo 
sudo apt install -y docker-ce
sudo apt install -y nodejs npm
sudo npm install -y npm --global
sudo apt install -y python3-pip git pwgen vim
sudo pip3 install -y requests==2.22.0
docker-compose version
sudo pip3 install docker-compose==1.25.0
sudo usermod -aG docker ${USER}
sudo pip3 install awscli
aws --version
curl -sfL https://get.k3s.io | sudo bash -
sudo chmod 644 /etc/rancher/k3s/k3s.yaml
kubectl get nodes
kubectl version --short
